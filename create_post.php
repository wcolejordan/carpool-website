<?php
require_once('menu.php');
header('Cache-control: private');
if(isset($_SESSION['user']))
{
?>
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html  xmlns="http://www.w3.org/1999/xhtml"> 
        <head>
            <title>Create Post</title>
        </head>
        <body>
        <h3> Create a post! </h3>
            <form method="post" action="handlers/manage_post_.php" enctype="multipart/form-data">
                <fieldset>
                    <table>
                        <tr> 
                            <td>
                                <label>Description</label>
                            </td>  
                            <td>
                                <textarea id="description" rows="4" cols="65" name="description"></textarea>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Start point </label>
                            </td>  
                            <td>
                                <input type="text" id="start_point" name="start_point"/>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>End point </label>
                            </td>  
                            <td>
                                <input type="text" id="end_point" name="end_point"/>     
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Outgoing time </label>
                            </td>  
                            <td>
                                <select name="outgoing">
                                    <option value="06:00">06:00</option>
                                    <option value="06:30">06:30</option>
                                    <option value="07:00">07:00</option>
                                    <option value="07:30">07:30</option>
                                    <option value="08:00">08:00</option>
                                    <option value="08:30">08:30</option>
                                    <option value="09:00">09:00</option>
                                    <option value="09:30">09:30</option>
                                </select>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Return time </label>
                            </td>  
                            <td>
                                <select name='return'>
                                    <option value="16:00">16:00</option>
                                    <option value="16:30">16:30</option>
                                    <option value="17:00">17:00</option>
                                    <option value="17:30">17:30</option>
                                    <option value="18:00">18:00</option>
                                    <option value="18:30">18:30</option>
                                    <option value="19:00">19:00</option>
                                    <option value="19:30">19:30</option>
                                </select>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Seats available </label>
                            </td>  
                            <td>
                                <select name="seats">
                                    <option value="N/A">N/A</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Days </label>
                            </td>  
                            <td> 
                                <label>Monday</label>  
                                <input type="checkbox" id="days1" name="days_checkbox[]" value="Monday"/>  
                                <br />
                                <label>Tuesday</label>
                                <input type="checkbox" id="days2" name="days_checkbox[]" value="Tuesday"/>   
                                <br />
                                <label>Wednesday</label>
                                <input type="checkbox" id="days3" name="days_checkbox[]" value="Wednesday"/>
                                <br />   
                                <label>Thursday</label>
                                <input type="checkbox" id="days4" name="days_checkbox[]" value="Thursday"/> 
                                <br />  
                                <label>Friday</label>
                                <input type="checkbox" id="days5" name="days_checkbox[]" value="Friday"/> 
                                <br />  
                                <label>Saturday</label>
                                <input type="checkbox" id="days6" name="days_checkbox[]" value="Saturday"/>   
                                <br />
                                <label>Sunday</label>
                                <input type="checkbox" id="days7" name="days_checkbox[]" value="Sunday"/> 
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Type </label>
                            </td>  
                            <td>
                                <label>Offering a lift</label> 
                                <input type="radio" id="type1" name="type" value="Offer" checked="checked"/> 
                                <br />
                                <label>Require a lift</label> 
                                <input type="radio" id="type2" name="type" value="Require"/>     
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Upload image(s) </label>
                            </td>  
                            <td>
                                <input type="file" id="file" name="file[]" multiple="multiple"/>     
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="Post" name="post"/>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Image text (Will be applied <br/> to all when uploading multiple <br/> files. Edit at 'My Posts') </label>
                            </td>  
                            <td>
                                <input type="text" id="text" name="img_text"/>     
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <?php 
                if(isset($_SESSION['message']))
                {
                ?>
                    <p> <?php echo $_SESSION["message"]; ?></p>
                <?php
                    unset($_SESSION['message']);
                }
                ?>
            </form>
        </body>
    </html>


<?php
}
else
{
    header("Location: login.php");
    exit();
}
?>
