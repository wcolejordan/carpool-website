<?php
require_once('menu.php');
require_once('classes/DbConnect.class.php');
header('Cache-control: private');
if(isset($_SESSION['user']))
{
?>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml"> 
    <head>
        <title>My Posts</title>
    </head>
    <body>
        <?php
        #initialize database class
        $db = new DbConnect();
        
        #get posts
        $username = $_SESSION['user'];
        $postquery = "SELECT * FROM posts WHERE username='$username' ORDER BY post_date LIMIT 50";
        $posts = $db -> select($postquery);
        if(count($posts) != 0)
        {
        ?>
        <p>
            To edit, delete or add/remove pictures to a post, click the date of the post to edit!
        </p>
        <table>
            <tr> 
                <td>
                    <label>Date Posted</label>
                </td>
                <td>
                    <label>Description</label>
                </td> 
                <td>
                    <label>Start point</label>
                </td>
                <td>
                    <label>End point</label>
                </td> 
                <td>
                    <label>Outgoing time </label>
                </td> 
                <td>
                    <label>Return time </label>
                </td>
            </tr>
            <?php         
            foreach($posts as $row)
            {
            ?>
                <!-- Format post data in a table by rows -->
                <tr>
                    <td>
                        <label> <a href='edit_post.php?id=<?php echo $row['postid'];?>'> <?php echo $row['post_date']; ?> </a></label>
                    </td>
                    <td>
                        <label> <?php echo $row['description']; ?> </label>
                    </td>
                    <td>
                        <label> <?php echo $row['start_point']; ?> </label>
                    </td>               
                    <td>   
                        <label> <?php echo $row['end_point']; ?></label>
                    </td>
                    <td>
                        <label> <?php echo $row['outtime']; ?> </label>
                    </td>
                    <td>
                        <label> <?php echo $row['returntime']; ?> </label>
                    </td>     
                </tr>
            <?php
            }
            ?>
        </table>
        <?php
        }
        else
        {
        ?>
            <p>
                You haven't yet created a post! Create one <a href='create_post.php'>here!</a>
            </p>
        <?php
        }
        ?>
        <?php 
        if(isset($_SESSION['message']))
        {
        ?>
            <p> <?php echo $_SESSION["message"]; ?></p>
        <?php
            unset($_SESSION['message']);
        }
        ?>
    </body>
    <?php
}
else
{
    header("Location: login.php");
    exit();
}
?>