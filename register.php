<?php 
require_once('menu.php');
header('Cache-control: private');
?>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml"> 
    <head>
        <title>Register</title>
    </head>
        <body>
<?php            
if(isset($_SESSION['user']))
{
?>
        
    <p> <?php echo 'You already have an account and are currently logged in!'; ?> </p>
<?php
}
else 
{ 
?>
        <h1> Register your account now! </h1>
            <form method="post" action="handlers/register_.php">
                <fieldset>
                    <table>
                        <tr> 
                            <td>
                                <label>Username: </label>
                            </td>  
                            <td>
                                <input type="text" id="username" name="username" 
                                        <?php if(isset($_SESSION['temp_user']))
                                        {
                                        ?>
                                            value="<?php echo $_SESSION['temp_user'] ?>"    
                                        <?php
                                        }
                                        unset($_SESSION['temp_user']);
                                        ?>
                                /> 
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Password: </label>
                            </td>  
                            <td>
                                <input type="password" id="password" name="password"/>     
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Confirm Password: </label>
                            </td>  
                            <td>
                                <input type="password" id="password_confirm" name="password_confirm"/>     
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Email: </label>
                            </td>  
                            <td>
                                <input type="text" id="email" name="email"
                                        <?php if(isset($_SESSION['temp_email']))
                                        {
                                        ?>
                                            value="<?php echo $_SESSION['temp_email'] ?>"    
                                        <?php
                                        }
                                        unset($_SESSION['temp_email']);  
                                        ?>
                                 /> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img id="captcha" src="/securimage/securimage_show.php" alt="CAPTCHA Image" />
                            </td>
                            <td>
                                <input type="text" name="captcha_code" size="10" maxlength="6" />
                                 <a href="#" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false">[ Different Image ]</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="Register" name="register"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <?php 
                if(isset($_SESSION['message']))
                {
                ?>
                    <p> <?php echo $_SESSION["message"]; ?></p>
                <?php
                    unset($_SESSION['message']);
                }
                ?>
            </form>
<?php
}
?>

    </body>
</html>
