<?php 
require_once('menu.php');
header('Cache-control: private');
?>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml"> 
    <head>
        <title>Login</title>
    </head>
        <body>
<?php            
if(isset($_SESSION['user']))
{
?>
    <p> <?php echo 'You are already logged in!'; ?> </p>
<?php
}
else 
{ 
?>
        <h1> Log in to get the full experience from our site </h1>
            <form method="post" action="handlers/login_.php">
                <fieldset>
                    <table>
                        <tr> 
                            <td>
                                <label>Username: </label>
                            </td>  
                            <td>
                                <input type="text" id="username" name="username"
                                        <?php if(isset($_SESSION['temp_user']))
                                        {
                                        ?>
                                            value="<?php echo $_SESSION['temp_user']; 
                                            unset($_SESSION['temp_user']);?>"   
                                        <?php
                                        }
                                        ?>
                                />
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Password: </label>
                            </td>  
                            <td>
                                <input type="password" id="password" name="password"/>     
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="Log in" name="login"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <p>No account? <a href="register.php"> Register Now!</a></p>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <?php 
                if(isset($_SESSION['message']))
                {
                ?>
                    <p> <?php echo $_SESSION["message"]; ?></p>
                <?php
                    unset($_SESSION['message']);
                }
                ?>
            </form>
<?php
}
?>
            
    </body>
</html>