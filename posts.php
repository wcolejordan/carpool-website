<?php
require_once('menu.php');
require_once('classes/Paginate.class.php');
header('Cache-control: private');
?>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml"> 
    <head>
        <title>Posts</title>
    </head>
    <body>
        <?php
        #set defaults
        $query = "SELECT * FROM posts ";

        #instantiate Paginate class
        $pg = new Paginate();

        #Set page depending on GET response
        $page = isset($_GET['page']) ? $pg -> sanitize($_GET['page']) : 1;
        if(isset($_POST['update']))
        {
            #order post does not require sanitization, handled below.
            $order = $_POST['orderby'];
            
            #Create correct ORDER BY sql arrangement
            if($order == "Oldest")
            {
               $_SESSION['orderby'] = "ORDER BY post_date ASC ";
            }
            else if($order == "Require")
            {
               $_SESSION['orderby'] = "WHERE type='Require' ORDER BY post_date DESC ";
            }
            else if($order == "Offer")
            {
                $_SESSION['orderby'] = "WHERE type='Offer' ORDER BY post_date DESC ";
            }
            else 
            {
                $_SESSION['orderby'] = "ORDER BY post_date DESC ";
            }
            
            
            $_SESSION['limit'] = $pg -> sanitize($_POST['limit']);
            $_SESSION['query'] = $query.$_SESSION['orderby'];
        }
        else
        {
            $_SESSION['query'] = $query."ORDER BY post_date DESC";
        }
        ?>
        <p>
        <?php 
        if(!isset($_SESSION['user']))
        { 
            echo "Please <a href='/login.php'>log in</a> or <a href='/register.php'> register</a> to search and view the users who created these posts!";
        }
        ?>
        </p>
        <form method="post" action="posts.php?page=<?php echo $page ?>" enctype="multipart/form-data">
            <fieldset>
                <table>
                    <tr>
                        <td>
                            <label>Order by:</label>
                        </td>
                        <td>
                            <select name="orderby">
                                <?php $temp_order = isset($_SESSION['orderby']) ? $order : ''; ?>
                                <option value="Newest" <?php echo $temp_order == "Newest" ? "selected='selected'" : ""; ?> >Newest</option>
                                <option value="Oldest" <?php echo $temp_order == "Oldest" ? "selected='selected'" : ""; ?> >Oldest</option>
                                <option value="Offer" <?php echo $temp_order == "Offer" ? "selected='selected'" : ""; ?> >Latest Offers</option>
                                <option value="Require" <?php echo $temp_order == "Require" ? "selected='selected'" : ""; ?> >Latest Requests</option>
                            </select>
                        </td>
                        <td>
                            <label>Results per page:</label>
                        </td>  
                        <td>
                            <select name="limit">
                                <?php $temp_limit = isset($_SESSION['limit']) ? $_SESSION['limit'] : ''; ?>
                                <option value="5" <?php echo $temp_limit == "5" ? "selected='selected'" : ""; ?> >5</option>
                                <option value="10" <?php echo $temp_limit == "10" ? "selected='selected'" : ""; ?> >10</option>
                                <option value="15" <?php echo $temp_limit == "15" ? "selected='selected'" : ""; ?> >15</option>
                                <option value="25" <?php echo $temp_limit == "25" ? "selected='selected'" : ""; ?> >25</option>
                                <option value="50" <?php echo $temp_limit == "50" ? "selected='selected'" : ""; ?> >50</option>
                                <option value="all" <?php echo $temp_limit == "all" ? "selected='selected'" : ""; ?> >All</option>
                            </select>
                        </td>
                        <td>
                            <input type="submit" value="Update" name="update"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <?php 
            if(isset($_SESSION['message']))
            {
            ?>
                <p> <?php echo $_SESSION["message"]; ?></p>
            <?php
                unset($_SESSION['message']);
            }
            ?>
        </form>
            
        <?php
        
        $limit = isset($_SESSION['limit']) ? $_SESSION['limit'] : 5;
        
        $pages = $pg -> _constructor($_SESSION['query'], $limit);
        if($pages < $page or $page < 1)
        {
            header("Location: /posts.php");
            exit();
        }
        echo $pg -> get_links($page);
        #get posts
        $posts = $pg -> select_limit();
        
        foreach($posts as $row)
        {
        ?>
            <!-- Format post data in a table -->
            <table>
                <?php 
                if(isset($_SESSION['user']))
                {
                ?>
                <tr> 
                    <td>
                        <label>Username</label>
                    </td>  
                    <td>
                        <label> <a href="/profile.php?user=<?php echo $row['username']; ?>"><?php echo $row['username']; ?></a></label>
                    </td>
                </tr>
                <?php
                }
                ?>
                <tr> 
                    <td>
                        <label>Date Posted</label>
                    </td>  
                    <td>
                        <label> <?php echo $row['post_date']; ?> </label>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <label>Description</label>
                    </td>  
                    <td>
                        <label> <?php echo $row['description']; ?> </label>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <label>Start point </label>
                    </td>  
                    <td>
                        <label> <?php echo $row['start_point']; ?> </label>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <label>End point </label>
                    </td>  
                    <td>   
                        <label> <?php echo $row['end_point']; ?></label>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <label>Outgoing time </label>
                    </td>  
                    <td>
                        <label> <?php echo $row['outtime']; ?> </label>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <label>Return time </label>
                    </td>  
                    <td>
                        <label> <?php echo $row['returntime']; ?> </label>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <label>Days</label>
                    </td>  
                    <td>
                        <label> <?php echo $row['days']; ?> </label>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <label>Type</label>
                    </td>  
                    <td> 
                        <label><?php echo $row['type']; ?> </label>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <label>Seats available</label>
                    </td>  
                    <td>
                        <label> <?php echo $row['seats_available']; ?> </label>  
                    </td>
                </tr>
                <?php
                $postid = $row['postid'];
                $imgquery = "SELECT * FROM images WHERE postid = '$postid'";
                $images = $pg -> select($imgquery);
                foreach ($images as $img)
                {
                    ?>
                <tr> 
                    <td>
                        <label><?php echo $img['text']; ?></label>
                    </td>  
                    <td>
                        <img width="300" src="<?php echo $img['path']; ?>" alt="<?php echo $img['text']; ?>">
                    </td>
                </tr>
                <?php
                }
                ?>
            </table>
            <br />
        <?php
        }
        ?>
    </body>
</html>