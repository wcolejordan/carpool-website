<?php
#begin session
session_start();
$path = dirname(__DIR__);
#Check submit button pressed and user logged in or redirect
if((isset($_POST['post']) || isset($_POST['update']) || isset($_POST['delete'])) && isset($_SESSION['user']))
{ 
    #Check for required inputs
    
    #import Images(extends DbConnect) and Locations files
    require_once("$path/classes/Locations.class.php");
    require_once("$path/classes/Images.class.php");
    
    #instantiate object
    $db = new Images();
    $locations = new Locations();
    
    #Check user has permissions to edit/delete current post    
    if(isset($_POST['delete']) || isset($_POST['update']))
    {
        $postid = $db->sanitize($_POST['postid']);
        $data = $db -> select("SELECT username FROM posts WHERE postid='$postid'");
        if($data[0]['username'] != $_SESSION['user'])
        {
            #ward off malicious users
            $_SESSION["message"] = "You shouldn't be doing that. We are watching you. Edit your own posts next time!";
            #redirect
            header("Location: /my_posts.php");
            exit();
        }
        if(isset($_POST['delete']))
        {
            #Get post id
            $postid = $_POST['postid'];
            
            #Generate query
            $delete = "DELETE FROM posts WHERE postid='$postid'";
            
            #delete and redirect
            $db -> query($delete);
            $_SESSION['message'] = "Post deleted";
            header("Location: /my_posts.php");
            exit();
        }
        
    }
    
    if(!isset($_POST['days_checkbox']) or $_POST['start_point'] == '' or $_POST['end_point'] == '' or $_POST['description'] == '')
    {
        #Check if post or update
        $imgreq = '';
        if(isset($_POST['post']))
        { 
            $imgreq = " (image not required)" ;
            $header = "/create_post.php";
        }
        else
        {
            #Get post id
            $postid = $_POST['postid'];
            $header = "/edit_post.php?id=$postid";
        }
        $_SESSION['message'] = 'Please fill out all fields'.$imagereq;
        header("Location: $header");
        exit();
    }
    
    
    #Get and sanitize inputs
    $days = $_POST['days_checkbox'];
    #format days checkbox
    $daystring = '';
    foreach($days as $temp)
    {
        $daystring = $daystring.', '.$db -> sanitize($temp);
    }
    $daystring = trim($daystring, ',');
    $description = $db -> sanitize(htmlentities($_POST['description']));
    $username = $_SESSION['user'];
    $start_point = $db -> sanitize($_POST['start_point']);
    $end_point = $db -> sanitize($_POST['end_point']);
    $outtime = $db -> sanitize($_POST['outgoing']);
    $return = $db -> sanitize($_POST['return']);
    $type = $db -> sanitize($_POST['type']);
    $seats = $db -> sanitize($_POST['seats']);
    $timestamp = date('Y-m-d H-i-s', time());
    
    #Get coordinates of locations
    $coords = $locations -> get_coords($start_point, $end_point);
    $start_lat = $coords['start_lat'];
    $start_lng = $coords['start_lng'];
    $end_lat = $coords['end_lat'];
    $end_lng = $coords['end_lng'];
    
    #if updating, update and exit
    if(isset($_POST['update']))
    { 
        $postid = $db -> sanitize($_POST['postid']);
        $update = "UPDATE posts SET description='$description', start_point='$start_point', end_point='$end_point', start_lat='$start_lat', start_lng='$start_lng', end_lat='$end_lat', end_lng='$end_lng', outtime='$outtime', returntime='$return', days='$daystring', type='$type', seats_available='$seats', post_date='$timestamp' WHERE postid='$postid'";
        $db -> query($update);
        $_SESSION['message'] = "Post updated successfully. <a href='my_posts.php'>Return to your posts</a>";
        header("Location: /edit_post.php?id=$postid");
        exit();
    }
    $imgtext = $db -> sanitize($_POST['img_text']);

    #generate postid
    $postid = bin2hex(random_bytes(20));
    #values for db input
    $values = "'$description'".','."'$start_point'".','."'$end_point'".','."'$start_lat'".','."'$start_lng'".','."'$end_lat'".','."'$end_lng'".','."'$outtime'".','."'$return'".','."'$daystring'".','."'$type'".','."'$seats'".','."'$timestamp'";


    #query string
    $insert = "INSERT INTO posts (postid, username, description, start_point, end_point, start_lat, start_lng, end_lat, end_lng, outtime, returntime, days, type, seats_available, post_date) VALUES ('$postid', '$username', $values)";
        
    #run standard post query
    $db -> query($insert);
    
    #Check if file has been set to upload and description text entered
    $total_files = count($_FILES['file']['name']);
    if($total_files > 0)
    {
        $check = $db -> post_image($total_files, $postid);
        if($check == false)
        {
            unset($_FILES["file"]);
            header("Location: /create_post.php");
            exit();
        }
        else
        {
            #clean up and redirect
            unset($_FILES["file"]);
            header("Location: /create_post.php");
            exit();
        }
    }
    else
    {
        #clean up and redirect
        $_SESSION['message'] = 'Posted successfully!';
        unset($_FILES["file"]);
        header("Location: /create_post.php");
        exit();
    }
}
else
{
    header("Location: /create_post.php");
    exit();
}
?>   