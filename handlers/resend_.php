<?php
$path = dirname(__DIR__);
if(isset($_POST['send']))
{
    session_start();
    
    #import DbConnect file
    require_once("$path/classes/DbConnect.class.php");
    
    #instantiate object
    $db = new DbConnect();
    
    //get data and sanitize where appropriate
    $username = $db -> sanitize($_POST['username']);
    $data = $db -> select("SELECT email, verified, verify_string FROM users WHERE username='$username'");

    if(count($data) > 0)
    {
        if($data[0]['verified'] == 'Y')
        {
            $_SESSION["message"] = "This account is already verified!";
            header("Location: /verify.php");
            exit;
        }
        else
        {
            $subject='Verify your Carpool Account Now!';
            $msg='Please follow the link to get verified: http://localhost/verify.php and use the activation code: '.$data[0]['verify_string'];
            mail($data[0]['email'],$subject,$msg);
            $_SESSION["message"] = 'Activation code sent.';
            header("Location: /verify.php");
            exit();
        }
    } 
    else
    {
        $_SESSION["message"] = 'This user is not registered.';
        header("Location: /verify.php");
        exit();
    }
}
else 
{
    header("Location: /verify.php");
    exit();
}

?>