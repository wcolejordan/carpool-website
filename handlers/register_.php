<?php
#begin session
session_start();
$path = dirname(__DIR__);
#Check submit button pressed and user logged in or redirect 
if(isset($_POST['register']) && !isset($_SESSION['user']))
{
    
    #import DbConnect file
    require_once("$path/classes/DbConnect.class.php");
    
    #instantiate object
    $db = new DbConnect();
    
    #Get and sanitize inputs
    $username = $db -> sanitize($_POST['username']);
    
    $email = $db -> sanitize($_POST['email']);
    $password = $db -> sanitize($_POST['password']);
    $password_confirm = $db -> sanitize($_POST['password_confirm']);
    $_SESSION['temp_user'] = $username;
    $_SESSION['temp_email'] = $email;

    #check username is alphanumeric
    if(!ctype_alnum($username))
    {
        $_SESSION["message"] = "Username can only contain letters and numbers.";
        header("Location: /register.php");
        exit();
    }
    
    #do the passwords match
    if($password != $password_confirm)
    {
        $_SESSION["message"] = 'Passwords do not match.';
        header("Location: /register.php");
        exit;
    }
    
    #ensure username less than 16
    if(strlen($username) > 16)
    {
        $_SESSION["message"] = 'Username must be 15 characters maximum.';
        header("Location: /register.php");
        exit;
    }
    
    #validate email format
    if(filter_var($email, FILTER_VALIDATE_EMAIL) != true)
    {
        $_SESSION["message"] = 'Email address not valid';
        header("Location: /register.php");
        exit;
    }

    #generate password hash
    $hash = password_hash($password, PASSWORD_DEFAULT);
    
    #Create query
    $select = "SELECT * FROM users WHERE ";
    
    #format options
    $username_str = "username='$username'";
    $email_str = "email='$email'";
    
    #execute queries
    $check_user = $db -> select($select.$username_str);
    $check_email = $db -> select($select.$email_str);
    
    #check username not taken
    if(count($check_user) > 0)
    {
        $_SESSION["message"] = 'Username already taken.';
        header("Location: /register.php");
        exit;
    } 
    #check email not taken
    else if (count($check_email) > 0) 
    {
        $_SESSION["message"] = 'Email already taken.';
        header("Location: /register.php");
        exit;
    }
    
    #Captcha code
    include_once $_SERVER['DOCUMENT_ROOT'] . '/securimage/securimage.php';
    $securimage = new Securimage();
    if ($securimage->check($_POST['captcha_code']) == false) {
        // the code was incorrect
        // you should handle the error so that the form processor doesn't continue

        // or you can use the following code if there is no validation or you do not know how
        $_SESSION["message"] = "The security code entered was incorrect.<br /><br />
        Please try again.";
        header("Location: /register.php");
        exit;
    }

    #create verify string
    $verify = bin2hex(random_bytes(3));
    $verify = substr($verify, 0, -1);
    
    #register statement
    $register = "INSERT INTO users (username, password, email, verify_string) VALUES ('$username', '$hash', '$email', '$verify')";
    
    #execute
    $db -> query($register);

    #email verification code
    $subject='Verify your Carpool Account Now!';
    $msg='Please follow the link to get verified: http://localhost/verify.php and use the activation code: '.$verify;
    mail($email,$subject,$msg);
    unset($_SESSION['temp_user']);
    unset($_SESSION['temp_email']);   
    header("Location: /verify.php");
    exit();
}
else
{
    header("Location: /register.php");
    exit();
}

?>