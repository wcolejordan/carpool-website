<?php
#Check submit button pressed or redirect
$path = dirname(__DIR__);
if(isset($_POST['login']))
{
    #begin session
    session_start();
    
    #import DbConnect file
    require_once("$path/classes/DbConnect.class.php");
    
    #instantiate object
    $db = new DbConnect();
    
    #sanitize inputs
    $username = $db->sanitize($_POST['username']);
    $password = $db->sanitize($_POST['password']);
    
    #query string
    $select = "SELECT password, verified FROM users WHERE username='$username'";
    
    #select from db
    $data = $db->select($select);

    #verify password matches
    if(password_verify($password, $data[0]['password']))
    {
        #check if user is verified
        if($data[0]['verified'] == 'Y')
        {
            #set session variable
            $_SESSION["user"] = $username;
            
            #get current time
            $timestamp = date('Y-m-d H-i-s', time());
            
            #update last_login field with current time
            $update = "UPDATE users SET last_login='$timestamp' WHERE username='$username'";
            
            #execute query and redirect to profile page
            $db->query($update);
            header("Location: /profile.php");
            exit;

        }
        else
        {
            #redirect to verify page with username
            $_SESSION["resend_user"] = $username;
            $_SESSION["message"] = 'Account not verified.';
            header("Location: /verify.php");
            exit();
        }
    } 
    else
    {
        #password_verify failed, store username to fill form and redirect to login
        $_SESSION["message"] = 'Username or password incorrect.';
        $_SESSION['temp_user'] = $username;  
        header("Location: /login.php");
        exit();
    }
}
else 
{
    header("Location: /login.php");
    exit();
}

?>