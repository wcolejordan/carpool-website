<?php
#begin session
session_start();
$path = dirname(__DIR__);
#Check submit button pressed and user logged in or redirect
if((isset($_POST['update']) || isset($_POST['upload']) || isset($_POST['delete'])) && isset($_SESSION['user']))
{ 
    #Check for required inputs
    
    #import Images(extends DbConnect) and Locations files
    require_once("$path/classes/Images.class.php");
    
    #instantiate object
    $db = new Images();
    
    
    
    #Check user has permissions to edit/delete current post    
    if(isset($_POST['delete']) || isset($_POST['update']))
    {
        $i = 0;
        foreach($_POST['image_checkbox'] as $image)
        {
            $imageid = $db -> sanitize($image);
            $postid = $db -> select("SELECT postid FROM images WHERE imageid='$imageid'");
            $data = $db -> select("SELECT username FROM posts WHERE postid="."'".$postid[0]['postid']."'");
            if($data[0]['username'] != $_SESSION['user'])
            {
                #ward off malicious users
                $_SESSION["message"] = "You shouldn't be doing that. We are watching you. Edit your own images next time!";
                #redirect
                header("Location: /my_posts.php");
                exit();
            } 
            
            if(isset($_POST['delete']))
            {
                $delete = "DELETE FROM images WHERE imageid='$imageid'";
                $db -> query($delete);  
            }
            else
            {
                $text = $_POST['img_text'.$i];
                $update = "UPDATE images SET text='$text' WHERE imageid='$imageid'";
                $db -> query($update);
            }
            $i++;
        }
        $_SESSION['message'] = isset($_POST['delete']) ? "Images deleted" : "Text updated";
        header("Location: /edit_post.php?id=".$postid[0]['postid']);
        exit();
    }
    else if(isset($_POST['upload']))
    {
        $postid = $db -> sanitize($_POST['postid']);
        $data = $db -> select("SELECT username FROM posts WHERE postid='$postid'");
        if($data[0]['username'] != $_SESSION['user'])
        {
            #ward off malicious users
            $_SESSION["message"] = "You shouldn't be doing that. We are watching you. Edit your own images next time!";
            #redirect
            header("Location: /edit_post.php?id=$postid");
            exit();
        } 
        $total_files = count($_FILES['file']['name']);
        $check = $db -> post_image($total_files, $postid);
        if($check == false)
        {
            unset($_FILES["file"]);
            header("Location: /edit_post.php?id=$postid");
            exit();
        }
        $_SESSION['message'] = 'Posted successfully!';
        header("Location: /edit_post.php?id=$postid");
        exit();
    }
        
}
else
{
    header("Location: /my_posts.php");
    exit();
}
?>   