<?php
#check submit button pressed or redirect
$path = dirname(__DIR__);
if(isset($_POST['activate']))
{
    #begin session
    session_start();
    
    #import DbConnect file
    require_once("$path/classes/DbConnect.class.php");
    
    #instantiate object
    $db = new DbConnect();
    
    #Get and sanitize inputs
    $verify = $db -> sanitize($_POST['verify']);

    #Create query
    $select = "SELECT verified FROM users WHERE verify_string='$verify'";
    
    #execute
    $data = $db -> select($select);
    
    #check not already verified
    if($data[0]['verified'] == 'N')
    {
        #Generate update query
        $update = "UPDATE users SET verified='Y', verify_string='' WHERE verify_string='$verify'";
        
        #execute
        $db -> query($update);
        
        #redirect
        $_SESSION["message"] = "Account verified successfully! You may now <a href='login.php'> log in!</a>";
        header("Location: /verify.php");
        exit();
    } 
    #if already verified
    else if($data['verified'] == 'Y')
    {
        $_SESSION["message"] = "This account is already verified, go and <a href='login.php'> log in!</a>";
        header("Location: /verify.php");
        exit();
    }
    #else redirect
    else
    {
        $_SESSION["message"] = 'Verification code is not valid';
        header("Location: /verify.php");
        exit();
    }
}
#check resend button pressed
else if($_POST['resend'])
{
    #begin session
    session_start();
    $_SESSION['resend'] = 'True';
    header("Location: /verify.php");
    exit();    
}
else
{
    header("Location: /verify.php");
    exit();
}


?>