<?php
session_start();
if(isset($_SESSION['user']))
{
?>
    <div class='topnav' id ='menu'>
        <a href="profile.php">My Profile</a>
        <a href="my_posts.php">My Posts</a>
        <a href="create_post.php">Create Post</a>
        <a href="posts.php">Posts</a>
        <a href="logout.php">Log out</a>
    </div>
<?php
}
else 
{
?>
    <div class='topnav' id ='menu'>
        <a href="login.php">Log in</a>
        <a href="verify.php">Verify</a>
        <a href="register.php">Register</a>
        <a href="posts.php">Posts</a>
    </div>
<?php
}
?>
