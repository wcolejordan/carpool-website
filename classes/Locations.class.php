<?php
class Locations
{
    public function get_coords($start, $end)
    {
        $path = dirname(__DIR__);
        require_once("$path/keys/googleapikey.php");
        
        #edit input for Google maps api call
        $start_call = urlencode($start_point); 
        $end_call = urlencode($end_point);

        #generate queries for Google maps api
        $google_api_call_start = "https://maps.googleapis.com/maps/api/geocode/json?address=$start_call,+England&key=$google_api_key";
        $google_api_call_end = "https://maps.googleapis.com/maps/api/geocode/json?address=$end_call,+England&key=$google_api_key";

        #Execute calls
        $temp = "https://maps.googleapis.com/maps/api/geocode/json?address=Greenwich+London,+England&key=$google_api_key";
        $start_response = file_get_contents($google_api_call_start);
        $end_response = file_get_contents($google_api_call_end);

        #parse json
        $json_start = json_decode($start_response, true);
        $json_end = json_decode($end_response, true);

        #allocate to variables
        $coords['start_lat'] = $json_start["results"][0]["geometry"]["location"]["lat"];
        $coords['start_lng'] = $json_start["results"][0]["geometry"]["location"]["lng"];
        $coords['end_lat'] = $json_end["results"][0]["geometry"]["location"]["lat"];
        $coords['end_lng'] = $json_end["results"][0]["geometry"]["location"]["lng"];
        
        return $coords;
    }
}