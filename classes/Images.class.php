<?php
require_once("DbConnect.class.php");
class Images extends DbConnect
{
    function post_image($total_files, $postid)
    {
        for($i=0; $i<$total_files; $i++)
        {
            if($_FILES['file']["tmp_name"][$i]!='' && $_POST['img_text'] != '')
            {
                #specify target location and filename
                $target = "../images/";
                $target_file = $target.bin2hex(random_bytes(5)).'-'.basename($_FILES["file"]["name"][$i]);
                $uploadOk = 1;

                #get type and size
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                $check = getimagesize($_FILES["file"]["tmp_name"][$i]);
                if($check !== false)
                {
                    $uploadOk = 1;
                }
                else
                {
                    $uploadOk = 0;
                }

                #check image type
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif")
                {
                    $uploadOk = 0;
                }

                #if upload not ok, redirect
                if($uploadOk == 0)
                {
                    $_SESSION['message'] = "File(s) could not be uploaded. Make sure you supply JPG, JPEG, GIF or PNG files.";
                    return false;
                }
                #if ok, attempt upload
                else
                {
                    #attempt upload, if fail, error message and redirect
                    if (!move_uploaded_file($_FILES["file"]["tmp_name"][$i], $target_file)) 
                    {
                        $_SESSION['message'] = print_r($_FILES);#"Sorry, there was an error uploading your file.";
                        return false;
                    } 
                    #If successful, add info to database of post and image
                    else
                    {
                        $imgtext = $this -> sanitize($_POST['img_text']);

                        #generate imageid
                        $imgid = bin2hex(random_bytes(20));

                        #image insert query
                        $insertimg = "INSERT INTO images (imageid, postid, path, text) VALUES ('$imgid', '$postid', '$target_file', '$imgtext')";

                        #run query
                        $this -> query($insertimg);
                    }
                }
            }
            else if ($_FILES['file']["tmp_name"][$i]!=''  && $_POST['img_text'] == '')
            {
                $_SESSION['message'] = "Please enter text to accompany the image(s) if you have chosen to upload one.";
                return false;
            }
            else if ($_FILES['file']["tmp_name"][$i]=='' && $_POST['img_text'] != '')
            {
                $_SESSION['message'] = "If you wish to supply image text you must first supply an image!";
                return false;
            }
            else 
            {
                $_SESSION['message'] = "Please select your image(s) and add some text.";
                return false;
            }
        }
        return true;
    }
    
}