<?php
require_once("DbConnect.class.php");
class Paginate extends DbConnect
{
    private $_query;
    private $_total;
    private $_limit;
    private $_page;
    
    function _constructor($query, $limit)
    {
        $this->_limit = $limit;
        $this->_query = $query;
        
        if($limit == 'all')
        {
            return 1;
        }
        else
        {
            $rows = $this->select($query);
            $total = count($rows);
            $this->_total = ceil($total / (int)$limit); 
            return $this->_total;
        }
    }
    
    function select_limit()
    {
        if($this->_limit == 'all') 
        {
            $result = $this -> select($this->_query);
            return $result;
        } 
        else
        {
            #formulate select query
            $result = $this -> select($this->_query." LIMIT ".(((int)$this->_page-1)*(int)$this->_limit).", $this->_limit");
            return $result;
        }
    }
    
    function get_links($page, $list_class="")
    {
        $this->_page = $page;
        #set max page links
        $links = 5;
        
        #check if lower
        if($this->_total < $links)
        {
            #set max pages
            $links = $this->_total;
        }
        if($this->_limit=='all')
        {
            return '';
        }
        
        #
        $start = (($this->_page - $links) > 0) ? $this->_page - $links : 1;
        $end = (($this->_page + $links) < $this->_total) ? $this->_page + $links : $this->_total;
        
        $html = "<table><tr class='".$list_class."'>";
        
        $class = ($this->_page == 1) ? "disabled" : "";
        $html .= '<td class="' . $class . '"><a href="?page=' . ( ($this->_page - 1) > 0 ? $this->_page - 1 : 1 ) . '">&laquo;</a></td>';
 
        if ($start > 1) 
        {
            $html .= '<td><a href="?page=1">1</a></td>';
            $html .= '<td class="disabled"><span>...</span></td>';
        }

        for ($i = $start; $i <= $end; $i++) 
        {
            $class = ($this->_page == $i) ? "active" : "";
            $html .= '<td class="'.$class .'"><a href="?page='.$i.'">'.$i.'</a></td>';
        }

        if ($end<$this->_total) 
        {
            $html .= '<td class="disabled"><span>...</span></td>';
            $html .= '<td><a href="?page=' . $this->_total . '">' . $this->_total . '</a></td>';
        }

        $class = ($this->_page == $this->_total) ? "disabled" : "";
        $html .= '<td class="'.$class.'"><a href="?page=' .( ($this->_page + 1) <= $this->_total ? $this->_page + 1 : $this->_page ).'">&raquo;</a></td>';
        $html .= '</tr></table>';

        return $html;

    }
    
}
?>