<?php
class DbConnect
{
    protected static $connection;
    #Connect to database, return mysqli instance on susccess
    public function connect()
    {
        #Try to connect
        if(!isset(self::$connection))
        {
            $path = dirname(__DIR__);
            require_once("$path/keys/dbcredentials.php");
            self::$connection = new mysqli($location, $username, $password, $db);
        }
        if(self::$connection == false)
        {
            #handle error
            return false;
        }
        return self::$connection;
    }
    
    #Query the database param:$query the query string, return:result of query
    public function query($query)
    {
        #Connect to database
        $connection = $this -> connect();
        
        #Query DB
        $result = $connection -> query($query);
        return $result;
    }
    
    #Fetch rows from DB (SELECT)
    public function select($query)
    {
        $rows = array();
        $result = $this -> query($query);
        #if nothing found
        if($result == false)
        {
            return false;
        }
        while ($row = $result -> fetch_assoc())
        {
            $rows[] = $row;
        }
        return $rows;
    }
    
    #Get Database errors
    public function error()
    {
        $connection = $this -> connect();
        return $connection -> error;
    }
    
    public function sanitize($value)
    {
        $connection = $this -> connect();
        return $connection -> real_escape_string($value);
    }
    
}
    
 
    

?>