<?php 
require_once('menu.php');
header('Cache-control: private');
?>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml"> 
    <head>
        <title>Verify</title>
    </head>
        <body>
<?php            
if(isset($_SESSION['user']))
{
?>
        
    <p> <?php echo 'You are already logged in!'; ?> </p>
<?php
}
else 
{ 
?>
        <h3> Please verify your account using the activation code sent to you by email. </h3>
            
            <form method="post" action="handlers/verify_.php">
                <fieldset>
                    <table>
                        <tr> 
                            <td>
                                <label>Activation Code: </label>
                            </td>  
                            <td>
                                <input type="text" id="verify" name="verify"/> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="Activate" name="activate"/>
                            </td>
                        </tr>
                        <tr>
                            <td>                                
                            </td>
                            <td>
                                <input type="submit" value="Resend Email" name="resend"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
            <?php
            if(isset($_SESSION['resend']))
            {
            ?>
            <p> <br /> </p>
            <form method="post" action="handlers/resend_.php">
                <fieldset>
                    <table>
                        <tr> 
                            <td>
                                <label>Username: </label>
                            </td>  
                            <td>
                                <input type="text" id="username" name="username"/> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="send" name="send"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
            <?php
                unset($_SESSION['resend']);
            }
            ?>
            <?php 
            if(isset($_SESSION['message']))
            {
            ?>
                <p> <?php echo $_SESSION["message"]; ?></p>
            <?php
                unset($_SESSION['message']);
            }
            ?>
<?php
}
?>
    </body>
</html>