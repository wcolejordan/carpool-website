<?php
require_once('menu.php');
header('Cache-control: private');
if(isset($_SESSION['user']) and isset($_GET['id']))
{
?>
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html  xmlns="http://www.w3.org/1999/xhtml"> 
        <head>
            <title>Edit Post</title>
        </head>
        <?php
        #import DbConnect Class
        require_once('classes/DbConnect.class.php');

        #instantiate object    
        $db = new DbConnect();

        #get postid and sanitize to protect from SQL injection
        $postid = $db -> sanitize($_GET['id']);
        $data = $db -> select("SELECT * FROM posts WHERE postid='$postid'");

        #Check user has permissions to edit/delete current post
        if($data[0]['username'] != $_SESSION['user'])
        {
            #ward off malicious users
            $_SESSION["message"] = "You shouldn't be doing that. We are watching you. Edit your own posts next time!";
            #redirect
            header("Location: /my_posts.php");
            exit();
        }
        ?>    
        <body>
        <h3> Edit your post </h3>
            <fieldset>
                <form method="post" action="handlers/manage_post_.php" enctype="multipart/form-data">
                    <table>
                        <tr> 
                            <td>
                                <label>Description</label>
                            </td>  
                            <td>
                                <textarea id="description" rows="4" cols="65" name="description"><?php echo $data[0]["description"];?></textarea>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Start point </label>
                            </td>  
                            <td>
                                <input type="text" id="start_point" name="start_point" value="<?php echo $data[0]["start_point"];?>"/>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>End point </label>
                            </td>  
                            <td>
                                <input type="text" id="end_point" name="end_point" value="<?php echo $data[0]["end_point"];?>"/>     
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Outgoing time </label>
                            </td>  
                            <td>
                                <select name="outgoing">
                                    <option <?php if($data[0]["outtime"] == "06:00"){ echo "selected='selected'"; } ?> value="06:00">06:00</option>
                                    <option <?php if($data[0]["outtime"] == "06:30"){ echo "selected='selected'"; } ?> value="06:30">06:30</option>
                                    <option <?php if($data[0]["outtime"] == "07:00"){ echo "selected='selected'"; } ?> value="07:00">07:00</option>
                                    <option <?php if($data[0]["outtime"] == "07:30"){ echo "selected='selected'"; } ?> value="07:30">07:30</option>
                                    <option <?php if($data[0]["outtime"] == "08:00"){ echo "selected='selected'"; } ?> value="08:00">08:00</option>
                                    <option <?php if($data[0]["outtime"] == "08:30"){ echo "selected='selected'"; } ?> value="08:30">08:30</option>
                                    <option <?php if($data[0]["outtime"] == "09:00"){ echo "selected='selected'"; } ?> value="09:00">09:00</option>
                                    <option <?php if($data[0]["outtime"] == "09:30"){ echo "selected='selected'"; } ?> value="09:30">09:30</option>
                                </select>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Return time </label>
                            </td>  
                            <td>
                                <select name="return">
                                    <option <?php if($data[0]["returntime"] == "16:00"){ echo "selected='selected'"; } ?> value="16:00">16:00</option>
                                    <option <?php if($data[0]["returntime"] == "16:30"){ echo "selected='selected'"; } ?> value="16:30">16:30</option>
                                    <option <?php if($data[0]["returntime"] == "17:00"){ echo "selected='selected'"; } ?> value="17:00">17:00</option>
                                    <option <?php if($data[0]["returntime"] == "17:30"){ echo "selected='selected'"; } ?> value="17:30">17:30</option>
                                    <option <?php if($data[0]["returntime"] == "18:00"){ echo "selected='selected'"; } ?> value="18:00">18:00</option>
                                    <option <?php if($data[0]["returntime"] == "18:30"){ echo "selected='selected'"; } ?> value="18:30">18:30</option>
                                    <option <?php if($data[0]["returntime"] == "19:00"){ echo "selected='selected'"; } ?> value="19:00">19:00</option>
                                    <option <?php if($data[0]["returntime"] == "19:30"){ echo "selected='selected'"; } ?> value="19:30">19:30</option>
                                </select>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Seats available </label>
                            </td>  
                            <td>
                                <select name="seats">
                                    <option <?php if($data[0]["seats_available"] == "N/A"){ echo "selected='selected'"; } ?> value="N/A">N/A</option>
                                    <option <?php if($data[0]["seats_available"] == "1"){ echo "selected='selected'"; } ?> value="1">1</option>
                                    <option <?php if($data[0]["seats_available"] == "2"){ echo "selected='selected'"; } ?> value="2">2</option>
                                    <option <?php if($data[0]["seats_available"] == "3"){ echo "selected='selected'"; } ?> value="3">3</option>
                                    <option <?php if($data[0]["seats_available"] == "4"){ echo "selected='selected'"; } ?> value="4">4</option>
                                    <option <?php if($data[0]["seats_available"] == "5"){ echo "selected='selected'"; } ?> value="5">5</option>
                                    <option <?php if($data[0]["seats_available"] == "6"){ echo "selected='selected'"; } ?> value="6">6</option>
                                    <option <?php if($data[0]["seats_available"] == "7"){ echo "selected='selected'"; } ?> value="7">7</option>
                                    <option <?php if($data[0]["seats_available"] == "8"){ echo "selected='selected'"; } ?> value="8">8</option>
                                </select>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Days </label>
                            </td>  
                            <td> 
                                <label>Monday</label>  
                                <input type="checkbox" id="days1" name="days_checkbox[]" value="Monday" <?php if(strpos($data[0]["days"], "Monday") !== FALSE) { echo "checked='checked'"; } ?>/>  
                                <br />
                                <label>Tuesday</label>
                                <input type="checkbox" id="days2" name="days_checkbox[]" value="Tuesday" <?php if(strpos($data[0]["days"], "Tuesday") !== FALSE) { echo "checked='checked'"; } ?>/>   
                                <br />
                                <label>Wednesday</label>
                                <input type="checkbox" id="days3" name="days_checkbox[]" value="Wednesday" <?php if(strpos($data[0]["days"], "Wednesday") !== FALSE) { echo "checked='checked'"; } ?>/>
                                <br />   
                                <label>Thursday</label>
                                <input type="checkbox" id="days4" name="days_checkbox[]" value="Thursday" <?php if(strpos($data[0]["days"], "Thursday") !== FALSE) { echo "checked='checked'"; } ?>/> 
                                <br />  
                                <label>Friday</label>
                                <input type="checkbox" id="days5" name="days_checkbox[]" value="Friday" <?php if(strpos($data[0]["days"], "Friday") !== FALSE) { echo "checked='checked'"; } ?>/> 
                                <br />  
                                <label>Saturday</label>
                                <input type="checkbox" id="days6" name="days_checkbox[]" value="Saturday" <?php if(strpos($data[0]["days"], "Saturday") !== FALSE) { echo "checked='checked'"; } ?>/>   
                                <br />
                                <label>Sunday</label>
                                <input type="checkbox" id="days7" name="days_checkbox[]" value="Sunday" <?php if(strpos($data[0]["days"], "Sunday") !== FALSE) { echo "checked='checked'"; } ?>/> 
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Type </label>
                            </td>  
                            <td>
                                <label>Offering a lift</label> 
                                <input type="radio" id="type1" name="type" value="Offer" <?php if($data[0]["type"] == "Offer") { echo "checked='checked'"; } ?>/> 
                                <br />
                                <label>Require a lift</label> 
                                <input type="radio" id="type2" name="type" value="Require" <?php if($data[0]["type"] == "Require") { echo "checked='checked'"; } ?>/>     
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
                                <input type="submit" value="Update" name="update"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="Delete Post" name="delete"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </fieldset>
            <h3>Upload image(s)</h3>
            <fieldset>
                <form method="post" action="handlers/manage_images_.php" enctype="multipart/form-data">
                    <table>
                        <tr> 
                            <td>
                                <label>Upload image(s) </label>
                            </td>  
                            <td>
                                <input type="file" id="file" name="file[]" multiple="multiple"/>     
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" value="Upload" name="upload"/>
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                <label>Image text (Will be applied <br/> to all when uploading multiple <br/> files. Edit at 'My Posts') </label>
                            </td>  
                            <td>
                                <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
                                <input type="text" id="text" name="img_text"/>     
                            </td>
                        </tr>
                    </table>
                </form>
            </fieldset>
            <?php
            $postid = $data[0]['postid'];
            $imgquery = "SELECT * FROM images WHERE postid = '$postid'";
            $images = $db -> select($imgquery);
            $i = 0;
            if(count($images)>0)
            {
            ?>
            <h3>Manage image(s)</h3>
            <fieldset>
                <form method="post" action="handlers/manage_images_.php" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <td>
                                <label>Text</label>
                            </td>
                            <td>
                                <label>Image</label>
                            </td>
                            <td>
                                <label>Delete</label>
                            </td>
                        </tr>
                    <?php
                    foreach ($images as $img)
                    {
                    ?>
                        <tr> 
                            <td>
                                <input type="text" id="img_text<?php echo $i ?>" name="img_text<?php echo $i ?>" value="<?php echo $img["text"];?>"/>
                            </td>  
                            <td>
                                <img width="300" src="<?php echo $img['path']; ?>" alt="<?php echo $img['text']; ?>">
                            </td>
                            <td>
                                <input type="checkbox" id="img_text<?php echo $i ?>" name="image_checkbox[]" value="<?php echo $img["imageid"];?>"/>
                            </td>
                        </tr>
                    <?php
                    $i++;
                    }
                    ?>   
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
                                <input type="submit" value="Update Selected" name="update"/>
                            </td>
                            <td>
                                <input type="submit" value="Delete Selected" name="delete"/>
                            </td>
                        </tr>                            
                    </table>
                </form>
            </fieldset>
            <?php
            }
            ?>

            <?php 
            if(isset($_SESSION['message']))
            {
            ?>
                <p> <?php echo $_SESSION["message"]; ?></p>
            <?php
                unset($_SESSION['message']);
            }
            ?>
        </body>
    </html>

<?php
}
else
{
    header("Location: my_posts.php");
    exit();
}
?>
